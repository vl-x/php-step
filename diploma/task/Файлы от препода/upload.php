<h3>Форма загрузки</h3>
<?php
if(!isset($_POST['uppbtn']))
{
?> 
<form action="index.php?page=2" method="post" enctype="multipart/form-data"> 
<div class="form-group">
    <label for="myfile">Выберите файл для загрузки:</label>
    <input type="hidden" name="MAX_FILE_SIZE" value="200000" />
    <input type="file" class="form-control" name="myfile" accept="image/*">
</div>
<button type="submit" class="btn btn-primary" name="uppbtn">Отправить</button>
</form>
<?php
}
else
{
   if(isset($_POST['uppbtn']))
   {
      // вывод ошибок
      if($_FILES['myfile']['error'] != 0) // если что-то пошло не так
      {
         echo "<h3/><span style='color:red;'>Код ошибки: " .$_FILES['myfile']['error']."</span><h3/>";
         exit(); // прекращаем дальнейшую работу
      }
     // проверяем есть ли временный путь
     if(is_uploaded_file($_FILES['myfile']['tmp_name'])) 
     {
        // переносим наш файл из внутренней директории
        move_uploaded_file($_FILES['myfile']['tmp_name'],"./images/".$_FILES['myfile']['name']);
        
     }
     echo "<h3/><span style='color:green;'>Файл загружен успешно!</span><h3/>";
   }
}	
?>