<?php 
$users = 'pages/users.txt';

function register($name, $pass, $email) // создаем функцию с тремя аргументами
{
	// валидация 
	$name=trim(htmlspecialchars($name)); 
	// обрезаем пробелы в начале и в конце строки 
	// и выполняем экранирование активных символов, чтобы исключить передачу како-го-либо скрипт
	$pass=trim(htmlspecialchars($pass));
	$email=trim(htmlspecialchars($email));

	if($name =='' || $pass =='' || $email =='') // проверка на пустое поле
	{ 
		echo "<h3/><span style='color:red;'>Не заполнены обязательные поля!</span><h3/>";
		return false;
	}

	if(strlen($name) < 3 || strlen($name) > 30 || strlen($pass) < 3 || strlen($pass) > 30) // проверка на длину символов
	{
		echo "<h3/><span style='color:red;'>Логин должны быть в пределах от 3 до 30!</span><h3/>";
		return false;	
	}

	// Проверяем уникальность логина
	global $users;
	$file=fopen($users,'a+'); // открываем файл, если его нет, то создаем его

	while($line=fgets($file, 128)) // читаем построчно
	{
		$readname=substr($line,0,strpos($line,':')); // Возвращает подстроку и  Возвращает позицию первого вхождения подстроки
		if($readname == $name) // делаем проверку
		{
			echo "<h3/><span style='color:red;'>Такой логин уже используется!</span><h3/>";
			return false;
		}
	}
	// добавление нового пользователя
	$line=$name.':'.md5($pass).':'.$email."\r\n";
	fputs($file,$line);
	fclose($file);
	return true;
	
}
?>
