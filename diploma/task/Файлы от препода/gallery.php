<h2>Галлерея</h2>
<form action='index.php?page=3' method='post'>
  <p>Выберите расширение которое хотите увидеть:</p>
  <select name='ext'>
  <?php
    $path = 'images/';
    if($dir = opendir($path)) // ищем папку и возвращаем указатель на нее
    {
      $ar = array();
      while (($file = readdir($dir)) !== false) // возвращает информацию о файлах, расположенных в заданной папке
      {
  	$fullname = $path . $file; // полный путь к файлу
  	$pos = strrpos($fullname, '.'); // достаем расширение Возвращает позицию последнего вхождения символа
  	$ext = substr($fullname, $pos+1); // достаем расширение
  	$ext= strtolower($ext); // преобразуем в нижний регистр
  	if( !in_array($ext, $ar) ) // Проверяем, присутствует ли в массиве значение
  	{
  		$ar[] = $ext;
  		echo "<option>" . $ext . "</option>";
  	}
      }
      closedir($dir);  // закрываем папку
  } 
  ?>
  </select>
  <input type="submit" name="submit" value="Показать картинки" class="btn btn-primary"/>
</form>
<br/>
<?php
	if(isset($_POST['submit']))
	{
		 $ext = $_POST['ext'];
		 $ar = glob($path . "*." . $ext); // Находит файловые пути, совпадающие с шаблоном
		 echo "<div class='panel panel-primary'>";
		 echo '<div class="panel-heading">';
     echo '<h3 class="panel-title">Gallery content</h3></div>';
		  foreach ($ar as $a)
		 {
		 	echo "<a href='" . $a . "' target='_blank'>
		 		<img src='" . $a . "' height='100px' border='0' alt='picture' class='img-polaroid'/>
			</a>";
		 }
		 echo "</div>";
	}
?>
