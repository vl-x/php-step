-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Сен 25 2017 г., 14:39
-- Версия сервера: 5.5.53
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `sumki`
--

-- --------------------------------------------------------

--
-- Структура таблицы `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `login` varchar(30) NOT NULL,
  `pass` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `roleid` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `admins`
--

INSERT INTO `admins` (`id`, `login`, `pass`, `email`, `roleid`) VALUES
(1, 'admin', 'admin', 'admin@admin', 1),
(2, 'login', 'login', 'login@login', 2),
(4, '$login', '$pass', '$email', 2),
(5, 'logina', 'logina', 'logina@logina', 2),
(6, 'logino', 'logino', 'logino@logino', 2),
(7, '4444', '4444', '4444@4444', 2),
(8, '555', '555', '555@555', 2),
(9, '333', '333', '333@333', 2),
(10, 'женя', 'женя', '111@111', 2),
(11, 'vl-x', 'vl-x', 'vl-x@vl-x', 2),
(12, 'Вадимка', '111', '111@111', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `menu_name` varchar(30) NOT NULL,
  `content` text NOT NULL,
  `page_id` int(11) NOT NULL,
  `position` int(3) NOT NULL,
  `visible` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `menu_name`, `content`, `page_id`, `position`, `visible`) VALUES
(1, 'Главная', '', 1, 1, 1),
(2, 'О магазине', 'Наш магазин очень скромный. Но бесспорно лучший', 2, 2, 1),
(3, 'Оплата и доставка', 'Акция: купите две сумки по цене трёх!', 3, 3, 1),
(4, 'Контакты', 'Контакт 1: бла-бла <br>\r\nКонтакт 2: бла-бла <br>', 4, 4, 1),
(5, 'Регистрация', '', 5, 5, 1),
(6, 'Вход', '', 6, 6, 1),
(7, 'Админка', '', 7, 7, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(255) NOT NULL,
  `img_link` varchar(255) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `product_name`, `img_link`, `price`) VALUES
(1, 'Jane Shilton', 'bag-01.png', 63.9),
(2, 'Eleganzza', 'bag-02.png', 254),
(3, 'Milana', 'bag-02.png', 45),
(4, 'Fabretti', 'bag-04.png', 124.5),
(5, 'Jane Shilton', 'bag-05.png', 63.1),
(6, 'Alessandro Birutti', 'bag-06.png', 189),
(7, 'Jane Shilton', 'bag-01.png', 63.9),
(8, 'Eleganzza', 'bag-02.png', 254),
(9, 'Milana', 'bag-03.png', 45);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
