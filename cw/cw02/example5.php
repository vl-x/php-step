<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php // Пример аргументы функции

    $func = 'say_hello_name'; // присваиваем переменной $func имя функции
    $func('Павел');  // вызываем функцию с параметром

    say_hello_name('Павел');
    say_hello_name('Павел');


    function say_hello_name($name) { // функция с параметром 
      echo "<h1>Привет $name</h1>";

  }
    
  ?>

</body>
</html>
