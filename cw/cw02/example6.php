<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php // Пример простой функции с аргументами по умолчанию
    function get_sum($a = 2, $b = 3) {
      echo $a + $b."<br>";
    }

    get_sum();    // выводит 5
    get_sum(2,5);  // 7
    get_sum(5) ;   // 8
  ?>

</body>
</html>
