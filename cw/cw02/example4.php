<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php // Пример область видимости

    function say_hello_name($name) { // функция с параметром 
      echo "<h1>Привет $name</h1>";
      $name = "Семен";

  }
    say_hello_name("Владимир"); // Привет Владимир
    $name = "Михаил";
    say_hello_name($name); // Привет Михаил
    echo $name; // Михаил

    
  ?>

</body>
</html>
