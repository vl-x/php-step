<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php 

    function simple_func() { // обычная функция
      $a = 0;
      echo $a++."<br>";
    }

    simple_func();
    simple_func();
    simple_func();

    function simple_static_func() { // функция со статическими переменными
      static $a = 0;
      echo $a++."<br>";
    }

    simple_static_func();
    simple_static_func();
    simple_static_func();

  ?>

</body>
</html>
