<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php // Пример 

    function say_hello_name(&$name) { // Передача аргументов по ссылке
      echo "<h1>Привет $name</h1>";
      $name = "Семен";

  }

    $name = "Михаил";
    say_hello_name($name); // Привет Михаил
//  say_hello_name(&$name); //  Ошибка
    echo $name; // Семен

    
  ?>

</body>
</html>
