<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php // Пример простой функции
    function get_sum() {
      $sum = 10 + 5;
      return $sum."<br>";
    }

    function say_hello() {
      echo "Hello World!<br>";
    }

    echo get_sum(); // выводит 15
    say_hello(); // выводит Hello World!
  ?>

</body>
</html>
