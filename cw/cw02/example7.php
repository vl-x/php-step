<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php // Пример область видимости

    function say_hello_name($name) { // Обращение к глобальной области видимости 1,2
      echo "<h1>Привет $name</h1>";
      //global $name;
      $GLOBALS['name'] = "Семен";
      $name = "Семен";

  }

    $name = "Михаил";
    say_hello_name($name); // Привет Михаил!
    echo $name; // Семен

    
  ?>

</body>
</html>
