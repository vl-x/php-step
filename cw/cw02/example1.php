<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Project_2</title>
</head>
<body>
 
  <?php 
  //  Объявляем логическую переменную

  $flag = TRUE;

  // Вызываем функцию, если переменная $flag равна TRUE
  //if ($flag) get_sum(); // Ошибка

  // Если переменная $flag равна TRUE, объявляем функцию 

  if ($flag) {
      function get_sum() { //  это функция, которая зависит от условий
        $sum = 20 + 15;
        echo $sum ."<br>";
      }
    }
  
  // Вызываем функцию, если переменная $flag равна TRUE
  if ($flag) get_sum(); // Ошибка 

  ?>

</body>
</html>
