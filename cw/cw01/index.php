<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>CW01 Владимир Кусенков</title>
</head>
<body>
	<div class="main">

		<?php //создаём простой массив
			$arr = array ("name", "age", "prof");

			echo $arr[2];
		?>
		<hr>

		<?php //создаём ассоциативный массив
			$arr = array ("name" => "Alex", "age" => "20", "prof" => "Doctor");

			echo "$arr[name]<br>";
			echo "$arr[age]<br>";
			echo "$arr[prof]<br>";

			print_r($arr); //вывод массива на экран ДЛЯ ОТЛАДКИ
			echo " - вывод массива на экран";
			echo "<br>";
			var_dump($arr); //вывод массива на экран c типом переменных ДЛЯ ОТЛАДКИ
			echo " - вывод массива на экран c типом переменных";
		?>
		<hr>

		<?php //используем цикл foreach для вывода ЗНАЧЕНИЙ каждого элемента ассоциативного массива.
			foreach ($arr as $value) {
				echo "Значение ".$value.".<br>";
			}

		?>
		<hr>

		<?php //используем цикл foreach для вывода КЛЮЧЕЙ и соответствующих ЗНАЧЕНИЙ каждого элемента ассоциативного массива.
			foreach ($arr as $key => $value) {
				echo "Ключ: ".$key."; значение ".$value.".<br>";
			}

		?>
		<hr>
	</div>
</body>
</html>