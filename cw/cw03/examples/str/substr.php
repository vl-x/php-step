<?php
//substr — Возвращает подстроку

    $rest = substr("abcdef", -1);  

    echo $rest."<br>";

    $rest = substr("abcdef", -2); 

    echo $rest."<br>";

    $rest = substr("abcdef", -3, 1);

    echo $rest."<br>";


$rest = substr("abcdef", 1);    // возвращает "bcdef"

$rest = substr("abcdef", 1, 3); // возвращает "bcd"

$rest = substr("abcdef", 0, 4); // возвращает "abcd"

$rest = substr("abcdef", 0, 8); // возвращает "abcdef"


// к отдельным символам можно обращаться с помощью фигурных скобок
$string = 'abcdef';
echo $string{0};                // выводит a
echo $string{3};                // выводит d
?>