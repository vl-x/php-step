﻿<?php

///substr_replace — Заменяет часть строки

    $var = 'ABCDEFGH:/MNRPQR/';
    echo "Оригинал: $var<hr />";

    /* Обе следующих строки заменяют всю строку $var на 'bob'. */
    echo substr_replace($var, 'bob', 0) . "<br />";
    echo substr_replace($var, 'bob', 0, strlen($var)) . "<br />";

    /* Вставляет 'bob' в начало $var. */
    echo substr_replace($var, 'bob', 0, 0) . "<br />";

    /* Обе следующих строки заменяют 'MNRPQR' в $var на 'bob'. */
    echo substr_replace($var, 'bob', 10, -1) . "<br />";
    echo substr_replace($var, 'bob', -7, -1) . "<br />";

    /* Удаляет 'MNRPQR' из $var. */
    echo substr_replace($var, '', 10, -1) . "<br />";
?>