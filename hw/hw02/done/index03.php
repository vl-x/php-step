<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW02/3 Владимир Кусенков</title>
</head>
<body>
	<div class="main">

		<?php

		function calc1($action, $a, $b) {

			if ($action == 'plus') {
				$rez = $a + $b;

				echo $rez;
			} elseif ($action == 'minus') {
				$rez = $a - $b;

				echo $rez;
			} elseif ($action == 'multiply') {
				$rez = $a * $b;

				echo $rez;
			} elseif ($action == 'divide') {
				$rez = $a / $b;

				echo $rez;
			} else echo "Первый параметр задан неверно";
			
		}
				
		calc1($action = 'plus', 2, 3); // 'plus', minus', 'multiply', 'divide'

		?>		
		<hr>
	</div>
</body>
</html>