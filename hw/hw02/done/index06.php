<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW02/6 Владимир Кусенков</title>
</head>
<body>
	<div class="main">

		<?php

		// вариант 1 - обычно
		function srt($a, $b) {
			for ($i=$a; $i < $b; $i++) {
				if ($i % 2) {
					echo $i." ";
				}
			}
		}

		srt($a = 10, $b = 35);

		?>		
		<hr>

		<?php

		//вариант 2 - рекурсивно
		function funct() {
			static $a = 10;
			static $b = 35;
			$a++;
			if ($a % 2) {
				echo "$a"." ";
			}
		}
		
		for ($i = 10; $i < ($b-1); $i++) {
			funct();
		}
		
		?>
		<hr>
	</div>
</body>
</html>