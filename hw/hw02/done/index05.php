<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW02/5 Владимир Кусенков</title>
</head>
<body>
	<div class="main">

		<?php

		function streamline($arr) {
			sort($arr);
			
			for ($i=0; $i < count($arr); $i++) { 
				echo $arr[$i]." ";
			}
		}
		
		$arr = array (1, 22, 5, 66, 3, 57);

		streamline($arr);

		?>		
		<hr>
	</div>
</body>
</html>