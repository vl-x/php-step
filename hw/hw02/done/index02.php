<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW02/2 Владимир Кусенков</title>
</head>
<body>
	<div class="main">

		<?php
			$arr = array (1, 2, 3, 4);
			$action = 'divide'; // 'plus', minus', 'multiply', 'divide'

		function calc($arr, $action) {
			

			if ($action == 'plus') {
				$rez = 0;

				foreach ($arr as $value) {
					$rez = $rez + $value;
				}

				echo $rez;
			} elseif ($action == 'minus') {
				$rez = 0;

				foreach ($arr as $value) {
					$rez = $rez - $value;
				}

				echo $rez;
			} elseif ($action == 'multiply') {
				$rez = 1;

				foreach ($arr as $value) {
					$rez = $rez * $value;
				}

				echo $rez;
			} elseif ($action == 'divide') {
				$rez = 1;

				foreach ($arr as $value) {
					$rez = $rez / $value;
				}

				echo $rez;
			} else echo "Второй параметр задан неверно";

		}
				
		calc($arr, $action);

		?>		
		<hr>
	</div>
</body>
</html>