<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW04 Владимир Кусенков</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<link rel="stylesheet" href="mystyle.css">
</head>

<?php 
	$action=isset($_GET['action'])?$_GET['action']:'login';

?>
<body>
	<div class="all-content">	
		<div class="jumbotron">
			<div class="container">
				<section class="tab-header">
					<div class="row">
						<div class="col-md-12">
							<ul class="nav nav-pills nav-justified">
								<li class="<?php echo $action=='login'?'active':'' ?>">
							    	<a data-toggle="tab" href="#item1">вход</a>
							    </li>
							    <li class="<?php echo $action=='reg'?'active':'' ?>">
							    	<a data-toggle="tab" href="#item2">регистрация</a>
							    </li>
							</ul>
						</div>
					</div>
				</section>
			</div>
		</div>



		<div class="container">
			<section class="tab-content clearfix">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="tab-content">	
							<div class="tab-pane <?php echo $action=='login'?'active':'' ?>" id="item1">
								<fieldset>
									<legend>вход</legend>
									<form action="login.php" method="post">
										<input type="text" name="login" placeholder="введите логин" value="<?php echo $login;?>">
										<span class="error"><?php echo $_GET['e1'];?></span>
										<br>
										<br>
										<input type="password" name="password" placeholder="введите пароль" value=<?php echo "'".$password."'";?>>
										<span class="error"><?php echo $_GET['e2'];?></span>
										<br>
										<br>
										<input type="hidden" name="go" value="5">
										<input type="submit" value="войти">
									</form>
								</fieldset>
							</div>
							<div class="tab-pane <?php echo $action=='reg'?'active':'' ?>" id="item2">
								<fieldset>
									<legend>регистрация</legend>
									<form action="submit.php" method="post">
										<input type="text" name="name" placeholder="введите имя" id="name" value=<?php echo "'".$name."'";?>>
										<label for="name">введите имя</label>
										<span class="error"><?php echo $_GET['e3'];?></span>
										<br>
										<br>
										<input type="text" name="surname" placeholder="введите фамилию" is="surname" value=<?php echo "'".$surname."'";?>>
										<label for="surname">введите фамилию</label>
										<span class="error"><?php echo $_GET['e4'];?></span>
										<br>
										<br>
										<input type="date" name="dob" placeholder="введите дату рождения" id="dob" value=<?php echo "'".$date."'";?>>
										<label for="dob">введите дату рождения</label>
										<span class="error"><?php echo $_GET['e5'];?></span>
										<br>
										<br>
										<input type="text" name="sex" placeholder="введите пол" id="sex" value=<?php echo "'".$sex."'";?>>
										<label for="sex">введите пол</label>
										<span class="error"><?php echo $_GET['e6'];?></span>
										<br>
										<br>
										<input type="text" name="login" placeholder="введите логин" id="login" value=<?php echo "'".$login."'";?>>
										<label for="login">введите логин</label>
										<span class="error"><?php echo $_GET['e7'];?></span>
										<br>
										<br>
										<input type="password" name="password" placeholder="введите пароль" id="password" value=<?php echo "'".$password."'";?>>
										<label for="password">введите пароль</label>
										<span class="error"><?php echo $_GET['e8'];?></span>
										<br>
										<br>
										<input type="hidden" name="go" value="6">
										<input type="submit" value="зарегистрироваться">
									</form>
								</fieldset>
							</div>
						</div>
					</div>
					<div class="col-md-4"></div>
				</div>
			</section>
		</div>
	</div>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/sticky/dist/sticky-kit.js"></script>
<script type="text/javascript" src="js/scrollfix.js"></script>
<script type="text/javascript" src="js/smooth-scroll.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/fitvids.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.min.js"></script>
<script type="text/javascript" src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

</body>
</html>