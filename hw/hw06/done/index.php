<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">	
	<meta http-equiv="X-UA-Compatible" content="IE=edge"> <!-- режим совместимости с IE-->
	<meta name="viewport" content="width=device-width, initial-scale=1"> <!-- Для правильного отображения на устройствах -->
	<title>DIPLOMA Владимир Кусенков</title>

	<!-- Bootstrap --> 
	<!-- <link href="css/bootstrap.min.css" rel="stylesheet"> -->
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
<div class="container">	
	<div class="row">
		<div class="col-sm-12 col-md-12 col-lg-12">
			
		</div>
	</div>
</div>

<div class="container">
	<div class="row">
		<nav class="col-sm-12 col-md-12 col-lg-12">
			<?php 
				include_once('pages/menu.php');  // подключаем меню
				include_once('pages/functions.php');  // подключаем файл с функциями
			?>
		</nav>
	</div>
</div>

<main>
	<div class="container"> <!-- данный див отвечает за контент необходимой страницы -->
		<div class="row">
			<div class="col-sm-12 col-md-12 col-lg-12">
				<?php
					if(isset($_GET['page'])) // проверяем на наличие переменной
					{
						$page = $_GET['page'];
						if($page == 1)include_once('pages/home.php');
						if($page == 2)include_once('pages/about.php');
						if($page == 3)include_once('pages/payment.php');
						if($page == 4)include_once('pages/contacts.php');
						if($page == 5)include_once('pages/registration.php'); //подключаем страницы согласно пунктов меню
					}
				?>
			</div>
		</div>
	</div>
</main>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				
			</div>
		</div>
	</div>
</section>
<footer>
	<div class="container">
		
	</div>
</footer>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 
<!-- <script src="js/bootstrap.min.js"></script> -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
</body>
</html>