<?php

$users = 'pages/users.txt';

function register($login, $pass, $pass2, $email) {// создаем функцию с тремя аргументами 
$errors = array();

	if(isset($_POST["regbtn"])) {
		
		//login	
		$login=trim($login);
		$login=strip_tags($login); // вырезаем теги
	        //конвертируем специальные символы в мнемоники HTML
		$login=htmlspecialchars($login,ENT_QUOTES);
	        /* на некоторых серверах
	         * автоматически добавляются
	         * обратные слеши к кавычкам, вырезаем их */
		$login=stripslashes($login);

		//pass	
		$pass=trim($pass);
		$pass=strip_tags($pass); // вырезаем теги
	        //конвертируем специальные символы в мнемоники HTML
		$pass=htmlspecialchars($pass,ENT_QUOTES);
	        /* на некоторых серверах
	         * автоматически добавляются
	         * обратные слеши к кавычкам, вырезаем их */
		$pass=stripslashes($pass);

		//pass2	
		$pass2=trim($pass2);
		$pass2=strip_tags($pass2); // вырезаем теги
	        //конвертируем специальные символы в мнемоники HTML
		$pass2=htmlspecialchars($pass2,ENT_QUOTES);
	        /* на некоторых серверах
	         * автоматически добавляются
	         * обратные слеши к кавычкам, вырезаем их */
		$pass2=stripslashes($pass2);

		//login	
		$email=trim($email);
		$email=strip_tags($email); // вырезаем теги
	        //конвертируем специальные символы в мнемоники HTML
		$email=htmlspecialchars($email,ENT_QUOTES);
	        /* на некоторых серверах
	         * автоматически добавляются
	         * обратные слеши к кавычкам, вырезаем их */
		$email=stripslashes($email);



		if ((strlen($login)<3)||(strlen($login)>30)) {
			$e1 = "Field 'Login' should contain not less 3 and not more 30 chars!<br>";
			$errors['e1'] = $e1;
			//echo $e1;
			//echo "<a href='index.php?page=5'>Click to return back to Registration</a>";
			//header("Location:index.php?page=5?e1=".$e1);
		} elseif (strlen($pass)==0) {
			$e2 = "Fill the field 'Password'<br>";
			$errors['e2'] = $e2;
			//echo $e2;
			//echo "<a href='index.php?page=5'>Click to return back to Registration</a>";
			//header("Location:index.php?page=5?e2=".$e2);
		} elseif (strlen($pass2)==0) {
			$e3 = "Fill the field 'Password'<br>";
			$errors['e3'] = $e3;
			//echo $e3;
			//echo "<a href='index.php?page=5'>Click to return back to Registration</a>";
			//header("Location:index.php?page=5?e3=".$e3);
		} elseif ($pass !== $pass2) {
			$e4 = "Passwords you entered did not match each other!<br>";
			$errors['e4'] = $e4;
			//echo "<a href='index.php?page=5'>Click to return back to Registration</a>";
			//header("Location:index.php?page=5&e4=".$e4);
		} elseif (strlen($email)==0) {
			$e5 = "Fill the field 'Email'<br>";
			$errors['e5'] = $e5;
			//echo $e5;
			//echo "<a href='index.php?page=5'>Click to return back to Registration</a>";
			//header("Location:index.php?page=5?e5=".$e5);
		} else {

			$handle = fopen("users.txt", 'a+');

			$is_login_exists = false;

			while (!feof($handle)) {
				$temp_string = fgets($handle);

				$temp_login = explode(":", $temp_string);


				if ($login == $temp_login[0]) {
					$e6 = "Login already exists!<br>";
					$errors['e6'] = $e6;
					//echo $e6;
					//echo "<a href='index.php?page=5'>Click to return back to Registration</a>";
					//header("Location:index.php?e6=".$e6);
					$is_login_exists = true;
					break;
				}

			}

			if ($is_login_exists == false) {
				fwrite($handle, $login.":".md5($pass).":".$email."\r\n"); // "\r\n" - перенос строки после окончания записи в файл users.txt

				echo "<h3/><span style='color:green;'>Registration succeed!</span><h3/>";
				echo "<a href='index.php?page=1'>Click to return back to Main</a>";
			} 

			fclose($handle);

		}

	}

return $errors;

}