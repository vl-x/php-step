<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW03 Владимир Кусенков</title>
</head>
<body>
	<div class="main">

		<?php
		echo "<p>Задание 1</p>";

		$a = 2;
		$b = 3.256;
		$c = 1.5;
		$d = -3.256;

		echo round($c);
		echo "<br>";

		echo round($c, 0, PHP_ROUND_HALF_DOWN);
		echo "<br>";

		echo ceil($b);
		echo "<br>";

		echo floor($b);
		echo "<br>";

		echo floor($d);
		echo "<br>";

		$arr = array (1, 3, 6, 12, 36);

		echo min($arr);
		echo "<br>";

		echo max($arr);
		echo "<br>";

		echo pow ($c, $a);
		echo "<br>";

		echo count($arr);
		echo "<br>";

		$arr1 = array (
			"a" => array (1, 3, 5),
			"b" => array (2, 4, 6),
			);

		echo count($arr1, COUNT_RECURSIVE);
		echo "<br>";

		?>
		<hr>
		<?php
		echo "<p>Задание 2</p>";

		$a1 = 25;

		$sin = sin($a1);

		$step = 2;
		$prerezult = pow($sin,$step);
		$result = ceil($prerezult);

		echo $result;
		echo "<br>";

		?>
		<hr>
		<?php
		echo "<p>Задание 3</p>";

		$login = dartveider;

		echo strlen($login);
		echo "<br>";
		echo substr($login, 0, 3);

		?>
		<hr>
		<?php
		echo "<p>Задание 4</p>";

		$str = "Hello, I'm Kitty";

		if (strlen($str) > 10) {
			$cut = substr_replace($str, ' ', 10);
			echo $cut;
		} else 
			echo "В строке менее 10 символов, куда уж меньше?!";

		echo "<br>";
		// делаем удаление символов a-g
		$a_to_g_symbols = array ("a", "b", "c", "d", "e", "f", "g");

		echo str_replace($a_to_g_symbols, "", $str);

		?>
		<hr>
		<p>Задание 5</p>
		<?php
		$text = '  
			<div class="refnamediv">
				<h1>htmlspecialchars</h1>
				<p> (PHP 4, PHP 5, PHP 7)</p>
				<p class="refpurpose"><span class="refname">htmlspecialchars</span> —
				<span class="dc-title">Любой текст</span></p>
			</div>
			';
		echo strip_tags($text);
		echo "<br>";
		echo strlen($text)." - количество символов до удаления тегов";
		echo "<br>";
		echo strlen(strip_tags($text))." - количество символов после удаления тегов";
		?>
		<hr>
		<?php
		echo "<p>Задание 7</p>";
		$a = 25;
		$b = 70;
		$c = 100;

		if (	((($a > 26) && ($a < $b)) || (($a = 25) && ($c == 100)) && ($b==70)) == true) {
			echo "OK";
		} else echo "bad";
		?>
	</div>
</body>
</html>