<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW01 Владимир Кусенков</title>
	<style>
		body > div > div {
			width: 100px;
			height: 100px;
			margin: 10px;
		}
	</style>
</head>
<body>
	<div class="main">

		<?php //создаём массив с цветами
			$arr = array ("red", "green", "yellow", "orange");

			shuffle($arr); /* перемешивание массива */
			
			$newArr = array_slice($arr, 0, 4); /* делаем срез из четырёх первых элементов перемешанного массива */

			//образец с занятия
			foreach ($newArr as $color) {
				echo '<div style="background-color:' . $color . ';"></div>';
			}

			echo "<hr>";

			//сделал сам через while
			$i = 1;
			$j = 0;
			while ($i < 2) {
				for ($j=0; $j < 4; $j++) { 
					echo '<div style="background-color:' . $newArr[$j] . ';"></div>';
				}
				$i++;
			}
		?>		
		<hr>
	</div>
</body>
</html>