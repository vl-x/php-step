<?php

if(isset($_POST["go"])) {
	
	//login	
	$login=trim($_POST["login"]);
	$login=strip_tags($login); // вырезаем теги
        //конвертируем специальные символы в мнемоники HTML
	$login=htmlspecialchars($login,ENT_QUOTES);
        /* на некоторых серверах
         * автоматически добавляются
         * обратные слеши к кавычкам, вырезаем их */
	$login=stripslashes($login);

	//email
	$email=trim($_POST["email"]);
	$email=strip_tags($email); // вырезаем теги
        //конвертируем специальные символы в мнемоники HTML
	$email=htmlspecialchars($email,ENT_QUOTES);
        /* на некоторых серверах
         * автоматически добавляются
         * обратные слеши к кавычкам, вырезаем их */
	$email=stripslashes($email);

	if ((strlen($login)<3)||(strlen($login)>30)) {
		$e1 = "Field 'Login' should contain not less 3 and not more 30 chars!<br>";
		header("Location:index.php?e1=".$e1);
	} elseif (strlen($email)==0) {
		$e2 = "Fill the field 'Email'<br>";
		header("Location:index.php?e2=".$e2);
	} else {

		$handle = fopen("users.txt", 'a+');

		

		while (!feof($handle)) {
			$temp_string = fgets($handle);

			$temp_login = explode(":", $temp_string);


			if ($login == $temp_login[0]) {
				$e3 = "Login already exists!<br>";
				header("Location:index.php?e3=".$e3);
				return false;
			}

		}

		fwrite($handle, $login.":".$email."\r\n"); // "\r\n" - перенос строки после окончания записи в файл users.txt

		echo "Registration succeed!<br>";
		echo "<a href='index.php'>Click to return back!</a>";

		
		fclose($handle);
	}

}