<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="UTF-8">
	<title>HW05 Владимир Кусенков</title>
	<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
	<link rel="stylesheet" href="js/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	<link rel="stylesheet" href="css/main.css">
</head>
<body>
	<div class="all-content">
		<div class="container">
			<div class="row">
				<div class="col-md-4"></div>
				<div class="col-md-4">
					<form action="handler.php" method="post">
						<fieldset>
							<legend>регистрация</legend>
							<input type="text" name="login" placeholder="введите пароль">
							<span class="error"><?php echo $_GET['e1'];?>
							<span class="error"><?php echo $_GET['e3'];?>	
							</span>
							<br>
							<br>
							<input type="email" name="email" placeholder="введите email">
							<span class="error"><?php echo $_GET['e2'];?></span>
							<br>
							<br>
							<input type="hidden" name="go">
							<input class="button" type="submit" value="регистрация">
							<br>
							<br>
						</fieldset>
					</form>
				</div>
				<div class="col-md-4"></div>
			</div>
		</div>	
	</div>

<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>  
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
<script type="text/javascript" src="js/sticky/dist/sticky-kit.js"></script>
<script type="text/javascript" src="js/scrollfix.js"></script>
<script type="text/javascript" src="js/smooth-scroll.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/fitvids.min.js"></script>
<script type="text/javascript" src="js/wow.min.js"></script>
<script type="text/javascript" src="js/jquery.blockUI.min.js"></script>
<script type="text/javascript" src="js/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
<script type="text/javascript" src="js/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

</body>
</html>